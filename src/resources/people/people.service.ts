import PeopleModel from '@/resources/people/people.model';
import People from '@/resources/people/people.interface';
import SeachQuery from './interfaces/people.searchquery.interface';
import { link, number, string } from 'joi';
import SearchQuery from './interfaces/people.searchquery.interface';



enum SortBy {
    asc = 1,
    desc = -1
}

class PeopleService {
    
    private people = PeopleModel;
    private default_country = "united states" // default country for now

    /** Search User  */
    public async searchByUserService(args: SearchQuery) : Promise<Object> {
        
        const { Client } = require('@opensearch-project/opensearch');
        const { OPENSEARCH_API } = process.env;
        var index_name = 'peoples-live-data';
        var index_new = 'leads';
        var client = new Client({ node: OPENSEARCH_API, });
        var mongoose = require('mongoose');

        let { full_name, first_name, last_name, linkedin_url, job_title, job_company_name, search_text, sortby, options, linkedin_username } = args; 
        let sortVal = sortby === "asc" ? SortBy.asc : SortBy.desc;
        let pipeline: any;
        pipeline = [];

        try{

                var query = {}
                var term = {}
                if(linkedin_url)
                {
                    linkedin_url = linkedin_url.substring(0, linkedin_url.length - +(linkedin_url.lastIndexOf('/')==linkedin_url.length-1));

                    console.log(linkedin_url);

                    query = { 'query': { 'term': { 'linkedin_url.keyword': linkedin_url }}}

                    var search = await client.search({
                        index: index_name,
                        body: query,
                        size: 1
                    });

                    var searchv2 = await client.search({
                        index: index_new,
                        body: query,
                        size: 1
                    });

                }
                else if(full_name)
                {
                    query = {
                        "query": {
                            "bool": {
                                "must": [{
                                        "term": {
                                            "full_name.keyword": full_name
                                        }
                                    },
                                    {
                                        "term": {
                                            "job_title.keyword": job_title
                                        }
                                    }
                                ]
                            }
                        }
                    }

                    console.log(query);

                    var search = await client.search({
                        index: index_name,
                        body: query,
                        size: 1
                    });

                    // var searchv2 = await client.search({
                    //     index: index_new,
                    //     body: query,
                    //     size: 1
                    // });
                }
                else
                {
                    query = {
                      "query": {
                        "bool": {
                          "filter": [
                            {
                              "exists": {
                                "field": "linkedin_url"
                              }
                            }
                          ]
                        }
                      }
                    }
                    console.log(query);
                    var search = await client.search({
                        index: index_name,
                        body: query,
                        size: 100
                    });

                    // var searchv2 = await client.search({
                    //     index: index_new,
                    //     body: query,
                    //     size: 1
                    // });

                    console.log(search);
                }

                

                var searchids : any;
                searchids = [];

                if(search.body.hits.hits)
                {
                    let hits = search.body.hits.hits;
                    console.log(hits);

                    hits.forEach(function(item: any){
                        pipeline.push({ $match: { _id: mongoose.Types.ObjectId(item._source.id)}});
                    });
                }
                  
                // if(pipeline.length === 0)
                // {
                //     if(searchv2.body.hits.hits)
                //     {

                //         let docs = searchv2.body.hits.hits;
                //         console.log(searchv2.body.hits.hits);

                //         return {docs: [docs[0]._source]};
                //     }
                //     return {docs : []};
                // }

                return {docs : []};
         
            let aggregate = this.people.aggregate(pipeline);

            const aggregatePaginate = await this.people.aggregatePaginate(aggregate, options)

            return aggregatePaginate;


        } catch (error) {
            console.log(error)
            throw new Error('Unable to get data');
        }
    }

    public async searchManyService(args: SearchQuery) : Promise<Object> {
        
        const { Client } = require('@opensearch-project/opensearch');
        const { OPENSEARCH_API } = process.env;
        var index_name = 'peoples-live-data';
        var index_new = 'leads';
        var client = new Client({ node: OPENSEARCH_API, });
        var mongoose = require('mongoose');

        let { full_name, first_name, last_name, linkedin_url, job_title, job_company_name, search_text, sortby, options, linkedin_username, linkedin_urls, users } = args; 
        let sortVal = sortby === "asc" ? SortBy.asc : SortBy.desc;
        let pipeline: any;
        pipeline = [];

        try{

                var query = {}
                var term = {}
                var must:any;
                must = [];

                if(linkedin_urls)
                {
                    let searchUrls = [];

                    linkedin_urls.forEach( function(url:any){
                        url = url.substring(0, url.length - +(url.lastIndexOf('/')==url.length-1));
                        must.push(url);
                    })

                    

                    query = {
                        "query": {
                            "terms": {"linkedin_url.keyword": must}
                        }
                    }
                    // console.log('test');
                    // console.log(JSON.stringify(query));
                }

                if(users)
                {
                    let fullNameTerms: any;
                    let JobTitleTerms: any;

                    fullNameTerms = []; 
                    JobTitleTerms = [];

                    console.log(users)
                    users.forEach( function(user:any){
                        console.log(user);
                        console.log(user.full_name);
                        fullNameTerms.push(user.full_name);
                        JobTitleTerms.push(user.job_title);
                    });
                    query = {
                        "query": {
                            "bool": {
                                "must": [{
                                        "terms": {
                                            "full_name.keyword": fullNameTerms
                                        }
                                    },
                                    {
                                        "terms": {
                                            "job_title.keyword": JobTitleTerms
                                        }
                                    }
                                ]
                            }
                        }
                    }

                    console.log(query);
                }

                var search = await client.search({
                    index: index_name,
                    body: query,
                    size: 10000
                });

                // var searchv2 = await client.search({
                //     index: index_new,
                //     body: query,
                //     size: 10000
                // });


                var searchids : any;
                searchids = [];

                if(search.body.hits.hits)
                {
                    let hits = search.body.hits.hits;
                    let inQuery:any;
                    inQuery = [];
                    let opensearchv2 = [];

                    // if(searchv2.body.hits.hits)
                    // {
                    //     opensearchv2 = searchv2.body.hits.hits;
                    // }

                    await hits.forEach(function(item: any){
                        // console.log(item);
                        inQuery.push(mongoose.Types.ObjectId(item._source.id));
                        // pipeline.push({ $match: { _id: mongoose.Types.ObjectId(item._source.id)}});
                    });
                    console.log(inQuery);
                    pipeline.push({ $match: { _id: { $in: inQuery}}});

                    // console.log(pipeline);
                    let aggregate = this.people.aggregate(pipeline);
                    const aggregatePaginate = await this.people.aggregatePaginate(aggregate, options);
                    // aggregatePaginate.opensearchv2 = opensearchv2;
                    return aggregatePaginate;
                }
                  
                if(pipeline.length === 0)
                {
                    return {docs : []};
                }

                
                return true;

            // console.log("aggregate pipeline");
            // console.log(aggregatePaginate);

            


        } catch (error) {
            console.log(error)
            throw new Error('Unable to get data');
        }
    }
    
    /** Insert And Update User Data */
    public async insertAndUpdateUserService(args: People): Promise <Object | void> {
        try {
            
            const { Client } = require('@opensearch-project/opensearch');
            const { OPENSEARCH_API } = process.env;
            var index_name = 'peoples-live-data';
            var client = new Client({ node: OPENSEARCH_API, });
            var mongoose = require('mongoose');
            const identifier = { linkedin_url: args.linkedin_url };
            var linkedin_url = args.linkedin_url;
            let pipeline: any;
            let dataInsert = [];
            pipeline = [];


            if(linkedin_url)
            {
                linkedin_url = linkedin_url.replace("https://www.", "");
                linkedin_url = linkedin_url.substring(0, linkedin_url.length - +(linkedin_url.lastIndexOf('/')==linkedin_url.length-1));
                console.log(linkedin_url);
            }
            

            var query = {}
            var term = {}
            if(linkedin_url)
            {
                linkedin_url = linkedin_url.substring(0, linkedin_url.length - +(linkedin_url.lastIndexOf('/')==linkedin_url.length-1));

                query = { 'query': { 'match': { 'linkedin_url.keyword': linkedin_url }}}
            }

            var search = await client.search({
                index: index_name,
                body: query,
                size: 1
            });

            var searchid = null;

            if(search.body.hits.hits)
            {
                let hits = search.body.hits.hits;

                hits.forEach(function(item: any){
                    if(linkedin_url == item._source.linkedin_url)
                    {
                        searchid = item._source.id;
                    } 
                });
            }

            let updateValues: any = {
                $set: {
                    image: args.image,
                    first_name: args.first_name,
                    middle_name: args.middle_name,
                    middle_initial: args.middle_initial,
                    last_name: args.last_name,
                    full_name: args.full_name,
                    gender: args.gender,
                    linkedin_url: linkedin_url,
                    linkedin_username: args.linkedin_username,
                    linkedin_id: args.linkedin_id,
                    industry: args.industry,
                    job_title: args.job_title,
                    job_company_name: args.job_company_name,
                    job_company_location_country: args.job_company_location_country,
                    job_company_location_continent: args.job_company_location_continent,
                    location_continent: args.location_continent,
                    location_country: args.location_country,
                    emails: args.emails,
                    work_email: args.work_email,
                    phone_numbers: args.phone_numbers,
                    mobile_numbers: args.mobile_numbers,
                    mobile_number: args.mobile_number,
                    isPiplSearch: args.isPiplSearch,
                    piplLatestSearch: args.piplLatestSearch,
                    source: args.source
                },
                $addToSet: {}, // this will update existing array or set a new 
            }

            if(args.skills && args.skills.length > 0){
                updateValues['$addToSet']['skills'] = { $each: args.skills };
            }
            if(args.profiles && args.profiles.length > 0){
                updateValues['$addToSet']['profiles'] = { $each: args.profiles };
            }
            if(args.education && args.education.length > 0){
                updateValues['$addToSet']['education'] = { $each: args.education };
            }
            if(args.interest && args.interest.length > 0){
                updateValues['$addToSet']['interest'] = { $each: args.interest };
            }
            if(args.experience && args.experience.length > 0){
                updateValues['$addToSet']['experience'] = { $each: args.experience };
            }

            let insertOrUpdate:any;

            // Insert or Update
            if(searchid === null)
            {
                let insertValue = {
                    image: args.image,
                    first_name: args.first_name,
                    middle_name: args.middle_name,
                    middle_initial: args.middle_initial,
                    last_name: args.last_name,
                    full_name: args.full_name,
                    gender: args.gender,
                    linkedin_url: linkedin_url,
                    linkedin_username: args.linkedin_username,
                    linkedin_id: args.linkedin_id,
                    industry: args.industry,
                    job_title: args.job_title,
                    job_company_name: args.job_company_name,
                    job_company_location_country: args.job_company_location_country,
                    job_company_location_continent: args.job_company_location_continent,
                    location_continent: args.location_continent,
                    location_country: args.location_country,
                    emails: args.emails,
                    work_email: args.work_email,
                    phone_numbers: args.phone_numbers,
                    mobile_numbers: args.mobile_numbers,
                    mobile_number: args.mobile_number,
                    skills: args.skills,
                    profiles: args.profiles,
                    education: args.education,
                    interest: args.interest,
                    experience: args.experience,
                    isPiplSearch: args.isPiplSearch,
                    piplLatestSearch: args.piplLatestSearch,
                    source: args.source
                }

                insertOrUpdate = await PeopleModel.create( insertValue );
            }
            else
            {
                insertOrUpdate = await PeopleModel.findOneAndUpdate( {_id : mongoose.Types.ObjectId(searchid)}, updateValues, { upsert: true, new: true, 'returnNewDocument':true });
            }


            let insert = { 
                linkedin_url: insertOrUpdate.linkedin_url,
                full_name : insertOrUpdate.full_name,
                first_name: insertOrUpdate.first_name,
                last_name: insertOrUpdate.last_name,
                linkedin_username: insertOrUpdate.linkedin_username,
                industry: insertOrUpdate.industry,
                emails: insertOrUpdate.emails,
                location_name: insertOrUpdate.location_name,
                id: insertOrUpdate._id,
                job_title: insertOrUpdate.job_title,
                job_company_name: insertOrUpdate.job_company_name,
            };

           dataInsert.push(insert);

           var response = await client.index({
                id: insertOrUpdate._id,
                index: index_name,
                body: insert,
                refresh: true
            })

            return insertOrUpdate;

        } catch (error) {
            console.log(error)
        }
    }

    public async insertAndUpdateUserManyService(args: People): Promise <Object | void> {
        try {
            
            const { Client } = require('@opensearch-project/opensearch');
            const { OPENSEARCH_API } = process.env;
            var index_name = 'peoples-live-data';
            var client = new Client({ node: OPENSEARCH_API, });
            var mongoose = require('mongoose');
            let multiInsert:any = args.users;
            let AllInsert: any = [];

            // console.log(multiInsert);

            await multiInsert.forEach(async function(user: any) {
                const identifier = { linkedin_url: user.linkedin_url };
                var linkedin_url = user.linkedin_url;
                let pipeline: any;
                let dataInsert = [];
                pipeline = [];
                console.log(args);
                if(linkedin_url)
                {

                    linkedin_url = linkedin_url.replace("https://www.", "");
                    linkedin_url = linkedin_url.substring(0, linkedin_url.length - +(linkedin_url.lastIndexOf('/')==linkedin_url.length-1));

                    console.log("linkedin_url", linkedin_url);
                    console.log("linkedin_url");
                }
                

                var query = {}
                var term = {}
                if(linkedin_url)
                {
                    linkedin_url = linkedin_url.substring(0, linkedin_url.length - +(linkedin_url.lastIndexOf('/')==linkedin_url.length-1));

                    query = { 'query': { 'term': { 'linkedin_url.keyword': linkedin_url }}}
                }

                var search = await client.search({
                    index: index_name,
                    body: query,
                    size: 1
                });

                var searchid = null;

                if(search.body.hits.hits)
                {
                    let hits = search.body.hits.hits;

                    hits.forEach(function(item: any){
                        if(linkedin_url == item._source.linkedin_url)
                        {
                            searchid = item._source.id;
                        } 
                    });
                }


                let updateValues: any = {
                    $set: {
                        image: user.image,
                        first_name: user.first_name,
                        middle_name: user.middle_name,
                        middle_initial: user.middle_initial,
                        last_name: user.last_name,
                        full_name: user.full_name,
                        gender: user.gender,
                        linkedin_url: linkedin_url,
                        linkedin_username: user.linkedin_username,
                        linkedin_id: user.linkedin_id,
                        industry: user.industry,
                        job_title: user.job_title,
                        job_company_name: user.job_company_name,
                        job_company_location_country: user.job_company_location_country,
                        job_company_location_continent: user.job_company_location_continent,
                        location_continent: user.location_continent,
                        location_country: user.location_country,
                        emails: user.emails,
                        work_email: user.work_email,
                        phone_numbers: user.phone_numbers,
                        mobile_numbers: user.mobile_numbers,
                        mobile_number: user.mobile_number,
                        isPiplSearch: user.isPiplSearch,
                        piplLatestSearch: user.piplLatestSearch,
                        source: user.source
                    },
                    $addToSet: {}, // this will update existing array or set a new 
                }

                console.log(updateValues);

                if(user.skills && user.skills.length > 0){
                    updateValues['$addToSet']['skills'] = { $each: user.skills };
                }
                if(user.profiles && user.profiles.length > 0){
                    updateValues['$addToSet']['profiles'] = { $each: user.profiles };
                }
                if(user.education && user.education.length > 0){
                    updateValues['$addToSet']['education'] = { $each: user.education };
                }
                if(user.interest && user.interest.length > 0){
                    updateValues['$addToSet']['interest'] = { $each: user.interest };
                }
                if(user.experience && user.experience.length > 0){
                    updateValues['$addToSet']['experience'] = { $each: user.experience };
                }

                let insertOrUpdate:any;

                // Insert or Update
                if(searchid === null)
                {
                    let insertValue = {
                        image: user.image,
                        first_name: user.first_name,
                        middle_name: user.middle_name,
                        middle_initial: user.middle_initial,
                        last_name: user.last_name,
                        full_name: user.full_name,
                        gender: user.gender,
                        linkedin_url: linkedin_url,
                        linkedin_username: user.linkedin_username,
                        linkedin_id: user.linkedin_id,
                        industry: user.industry,
                        job_title: user.job_title,
                        job_company_name: user.job_company_name,
                        job_company_location_country: user.job_company_location_country,
                        job_company_location_continent: user.job_company_location_continent,
                        location_continent: user.location_continent,
                        location_country: user.location_country,
                        emails: user.emails,
                        work_email: user.work_email,
                        phone_numbers: user.phone_numbers,
                        mobile_numbers: user.mobile_numbers,
                        mobile_number: user.mobile_number,
                        skills: user.skills,
                        profiles: user.profiles,
                        education: user.education,
                        interest: user.interest,
                        experience: user.experience,
                        isPiplSearch: user.isPiplSearch,
                        piplLatestSearch: user.piplLatestSearch,
                        source: user.source
                    }

                    insertOrUpdate = await PeopleModel.create( insertValue );
                }
                else
                {
                    insertOrUpdate = await PeopleModel.findOneAndUpdate( {_id : mongoose.Types.ObjectId(searchid)}, updateValues, { upsert: true, new: true, 'returnNewDocument':true });
                }


                let insert = { 
                    linkedin_url: insertOrUpdate.linkedin_url,
                    full_name : insertOrUpdate.full_name,
                    first_name: insertOrUpdate.first_name,
                    last_name: insertOrUpdate.last_name,
                    linkedin_username: insertOrUpdate.linkedin_username,
                    industry: insertOrUpdate.industry,
                    emails: insertOrUpdate.emails,
                    location_name: insertOrUpdate.location_name,
                    id: insertOrUpdate._id,
                    job_title: insertOrUpdate.job_title,
                    job_company_name: insertOrUpdate.job_company_name,
                };

               dataInsert.push(insert);

               var response = await client.index({
                    id: insertOrUpdate._id,
                    index: index_name,
                    body: insert,
                    refresh: true
                })

                AllInsert.push(insertOrUpdate);

                console.log(AllInsert);
                if(multiInsert[multiInsert.length-1] === user){
                  return AllInsert;
                }
            });

            console.log(AllInsert);
        
            // return AllInsert;
            

        } catch (error) {
            console.log(error)
        }
    }
    

    /** Archived Or Restore User by linkedin_url */
    public async archivedOrRestoreUserService(args: any): Promise <Object | any> {
        
        try {
        
            const filter = { linkedin_url: args.linkedin_url };

            let update;

            if(args.type === 'restore'){
                update = { $unset: { archive: 1 }};
            }
            
            if(args.type === 'archive'){
                update = { $set: { archive: true  }};
            }

            const options = { new: true };

            // const result = await PeopleModel.deleteOne({ linkedin_url });
            const result = await PeopleModel.findOneAndUpdate(filter, update, options);

            return result;

            
        } catch (error) {
            console.log(error)
        }
    }



    /** Get All Archive User  */
    public async getArchiveUserService(args: SearchQuery) : Promise<Object> {
    
        let { sortby, options } = args; 

        let sortVal = sortby === "asc" ? SortBy.asc : SortBy.desc;

        try {

            let pipeline = [
                { 
                    $match: { 
                        location_country: this.default_country,
                        archive: true
                    } 
                },
                { $sort: { _id: sortVal } },
                { $project: {
                        _id: 1,
                        linkedin_id: 1,
                        first_name: 1,
                        last_name: 1, 
                        full_name: 1, 
                        gender: 1,
                        industry: 1,
                        job_title: 1,
                        job_company_name: 1,
                        location_continent: 1,
                        location_country: 1, 
                        linkedin_url: 1 
                    } 
                }
            ];
         
            const aggregate = this.people.aggregate(pipeline);

            const aggregatePaginate = await this.people.aggregatePaginate(aggregate, options)

            return aggregatePaginate;

        } catch (error) {
            console.log(error)
            throw new Error('Unable to get data');
        }
    }

      
    /** Insert Imported JSON from excel/csv */
    public async insertExcelDataService (args: SearchQuery): Promise<Object>{

        let { excel_data, columns_to_fields } = args;

        try {
            
            // this are the fields that are type array in the database
            const fieldsArray = ["emails", "phone_numbers", "mobile_numbers","experience", "skills", "interest", "profiles", "education"];

            let final_to_store:any = [];
            let linkedin_url_arr:any = [];

            if(excel_data && columns_to_fields)
            {
                // map through excel data
                // console.log("excel_data: ", excel_data)
                excel_data.map((excel, index) => 
                {   
                    let newObj: any = {}; // placeholder if new field to values
                    let fields_key_obj: any = {
                        "emails": [],
                        "phone_numbers": [],
                        "mobile_numbers": [],
                        "experience":[],
                        "skills": [],
                        "interest": [],
                        "profiles": [],
                        "education": []
                    };

                    // get the key and val
                    for (const [key, val] of Object.entries(excel)) 
                    {
                        // compare the key and column here to set field and values to store
                        columns_to_fields.map((col: any) => {
                            // console.log("inside map: ", col, val)
                            if(col.column === key)
                            {   
                                if(col.set_field === "linkedin_url"){   
                                    if(!linkedin_url_arr.includes(val)){
                                        linkedin_url_arr.push(val);
                                    }
                                }

                                if(val === "")
                                {
                                    newObj[col.set_field] = ""; // empty cell in column
                                }
                                else if(fieldsArray.includes(col.set_field))
                                {
                                    let setVal = val;
                                    if(typeof setVal !== 'string' && typeof setVal === 'number')
                                    {
                                        fields_key_obj[col.set_field].push(String(setVal));
                                        newObj[col.set_field] = fields_key_obj[col.set_field];
                                    }
                                    else
                                    {
                                        if(setVal != "")
                                        {
                                            // check if value is comma separated
                                            if((<string>setVal).indexOf(',') != -1 )
                                            {
                                                // get the comma separated values and push it to array.
                                                let commaSeparatedArray = (<string>setVal).toString().replace(" ", "").split(",");
                                                commaSeparatedArray.map((sepval: any) => fields_key_obj[col.set_field].push(sepval));
                                                newObj[col.set_field] = fields_key_obj[col.set_field];
                                            }
                                            else
                                            {
                                                // just push value if not comma separated values
                                                fields_key_obj[col.set_field].push((<string>setVal).replace(" ", ""));
                                                newObj[col.set_field] = fields_key_obj[col.set_field];
                                            }
                                            // newObj[col.set_field] = (<string>setVal).toString().split(",") // creating objects field:value
                                        }
                                        else
                                        {
                                            newObj[col.set_field] = "";
                                        }
                                    }
                                }
                                else
                                {
                                    console.log('RUN 3')
                                    newObj[col.set_field] = val; // creating objects field:value
                                }
                            }
                        })

                        // add field:value default by united states
                        newObj["location_country"] = this.default_country;
                    }
                    // push to final variable to store
                    final_to_store.push(newObj);
               })

            }

            // params to return
            let data: any  = {
                linkedin_urls: [],
                inserts: [],
            };
      
            // Check if linkedin_url already exists in the collection
            if(linkedin_url_arr.length > 0){
                let exists = await this.people.find({ "linkedin_url": { "$in": linkedin_url_arr } }, { linkedin_url: 1 });
                if(exists.length > 0){   
                    data.linkedin_urls = exists;
                    return data;
                }
            }

            // console.log(final_to_store);
            // return {};

            // Insert All the excel data
            const insertParams: Array<any> = final_to_store;
            const insertMany = await this.people.insertMany(insertParams);
            data.inserts = insertMany;

            return data;

        } catch (error) {
            console.log(error);
            throw new Error('Unable to save imported data');
        }
    }


    /** Summary  */
    public async getSummary(): Promise <Object | void> {
        try {
            
            const pipeline = [{ $count: "Total" }]

            const summary = await PeopleModel.aggregate(pipeline);

            return summary;

        } catch (error) {
            console.log(error)
        }
    }


}

export default PeopleService;