import { Router, Request, Response, NextFunction } from 'express';
import queryString from 'querystring';
import Controller from '@/utils/interfaces/controller.interface';
import HttpException from '@/utils/exceptions/http.exception';
import validationMiddleware from '@/middleware/validation.middleware';
import validate from '@/resources/post/post.validation';
import PeopleService from '@/resources/people/people.service';
import SearchQuery from './interfaces/people.searchquery.interface';
import authenticated from '@/middleware/authenticated.middleware';


interface SearchUser {
    first_name: string;
    last_name: string;
    linkedin_url: string;
}

class PeopleController implements Controller {
    public path = '/datacenter';
    public router = Router();
    private PeopleService = new PeopleService();

    constructor() {
        this.initialiseRoutes();
    }

    private initialiseRoutes(): void {
        // inject validation if needed
        console.log('ALLO');
        this.router.get(`${this.path}/user`, authenticated, this.searchByUser);
        this.router.post(`${this.path}/user`, authenticated, this.insertAndUpdateUser);
        this.router.get(`${this.path}/user/archive`, authenticated, this.getAllArchiveUser);
        this.router.post(`${this.path}/user/archive-or-restore`, authenticated, this.archivedOrRestoreUser);
        this.router.post(`${this.path}/user/insert-excel`, authenticated, this.insertExcelData);
        this.router.get(`${this.path}/user/summary`, authenticated, this.getSummary);
        this.router.post(`${this.path}/user/search/many`, authenticated, this.searchMany);
        this.router.post(`${this.path}/user/insert/many`, authenticated, this.insertAndUpdateUserMany);
    }
    
    private searchByUser = async (req: Request, res: Response, next: NextFunction): Promise<Response | void> => {

        console.log(req.query);
        try {
            
            const { full_name, linkedin_username, first_name, last_name, job_title, job_company_name, linkedin_url, search_text, page = 1, limit = 20, sortby = 'desc', archive } = req.query as any;

            const searchParams: SearchQuery = {
                linkedin_url,
                full_name,
                first_name,
                last_name,
                job_title,
                job_company_name,
                linkedin_username,
                search_text,
                sortby,
                archive, 
                options: {
                    page,
                    limit,
                }
            }

            console.log(searchParams);

            if(linkedin_url && search_text) { next(new HttpException(400, 'Invalid parameters given')); }

            const data: any = await this.PeopleService.searchByUserService(searchParams);
            
            if(data.docs.length === 0){
                // requested by ms teff success if data is null 
                res.status(200).json({ data: [], message: "User not found" })
            }else{

                if(!data.docs[0].isPiplSearch)
                {
                    data.docs[0].isPiplSearch = false;
                }
                if(! data.docs[0].piplLatestSearch)
                {
                     data.docs[0].piplLatestSearch = "";
                }

                res.status(200).json({ data });
            }
         
        } catch (error) {
            console.log(error)
            next(new HttpException(400, 'Cannot make a search something went wrong.'));            
        }
    }

    private searchMany = async (req: Request, res: Response, next: NextFunction): Promise<Response | void> => {
        try {
            
            let args = req.body;

            let options:any = {limit: 100}

            args.options = options;
            const data:any = await this.PeopleService.searchManyService(args);

            let users = args.users;
            let linkedin_urls = args.linkedin_urls;

            let newDocs:any = [];
            let dataDocs = data.docs;
            let opensearchv2 = data.opensearchv2;

            if(users)
            {
                users.forEach( function(user:any){
                    let InsertData = {}

                    dataDocs.forEach( function(data:any){
                        
                        if(data.full_name == user.full_name && data.job_title == user.job_title)
                        {
                            InsertData = data;
                        }
                    });

                    newDocs.push(InsertData);
                });
            }
            if(linkedin_urls)
            {
                linkedin_urls.forEach( function(user:any, key:any){
                    let InsertData = {}
                    dataDocs.forEach( function(data:any){
                       user = user.substring(0, user.length - +(user.lastIndexOf('/')==user.length-1)); 
                        if(data.linkedin_url == user)
                        {
                            if(!data.isPiplSearch)
                            {
                                data.isPiplSearch = false;
                            }
                            if(!data.piplLatestSearch)
                            {
                                data.piplLatestSearch = "";
                            }

                            if(data.source)
                            {
                                if(data.source !== "zohorecruit")
                                {
                                    InsertData = data;
                                }
                            }
                            else
                            {
                                InsertData = data;
                            }

                            
                        }
                    });

                    newDocs[key] = InsertData;
                });

                // linkedin_urls.forEach( function(user:any, key:any){
                //     console.log(user);
                //     opensearchv2.forEach( function(data:any){
                //         user = user.substring(0, user.length - +(user.lastIndexOf('/')==user.length-1)); 
                //         let InsertData = {}
                //         if(data._source.linkedin_url == user)
                //         {
                //             InsertData = data._source;
                //             console.log('InsertData', InsertData);
                //             console.log('newDocs[key]', newDocs[key]);
                //             if(Object.keys(newDocs[key]).length === 0)
                //             {
                //                 console.log("IN");
                //                 // InsertData.isPiplSearch = false;
                //                 // InsertData.piplLatestSearch = "";
                //                 newDocs[key] = InsertData;
                //             }
                //         }
                //     })
                // });
            }

            data.opensearch = dataDocs;
            data.docs = newDocs;
            res.status(200).json({ data });

        } catch (error) {
             console.log(error)
            next(new HttpException(400, 'Users not found'));  
        }
    }

    /** Insert And Update User Data */
    private insertAndUpdateUser = async (req: Request, res: Response, next: NextFunction): Promise<Response | void> => {
        try {
            
            let args = req.body;

            const data = await this.PeopleService.insertAndUpdateUserService(args);

            res.status(200).json({ data });

        } catch (error) {
             console.log(error)
            next(new HttpException(400, 'User Cannot be inserted or updated, something went wrong'));  
        }
    }

    private insertAndUpdateUserMany = async (req: Request, res: Response, next: NextFunction): Promise<Response | void> => {
        try {
            
            let args = req.body;

            const data = await this.PeopleService.insertAndUpdateUserManyService(args);

            res.status(200).json({ data });

        } catch (error) {
             console.log(error)
            next(new HttpException(400, 'Users Cannot be insert or updated, something went wrong'));  
        }
    }


    /** Archived Or Restore User by linkedin_url */
    private archivedOrRestoreUser = async (req: Request, res: Response, next: NextFunction): Promise<Response | void> => {
        try {
            
            const { linkedin_url, type } = req.body;
    
            const params = {
                linkedin_url,
                type
            }

            const data: any = await this.PeopleService.archivedOrRestoreUserService(params);

            if(data === null) {
                next(new HttpException(400, 'Cannot arhived or restore this user, something went wrong'));
            }else{
                res.status(200).json({ status: 200, data, message: `User is ${type} successfully` });
            }
        
        } catch (error) {
            console.log(error);
            next(new HttpException(400, 'Cannot arhived or restore this user, something went wrong'));
        }
    }


    /* Get All Archive Users */
    private getAllArchiveUser = async (req: Request, res: Response, next: NextFunction): Promise<Response | void> => {
        try {
            
            const { page, limit, sortby = 'desc', archive } = req.query as any;

            const searchParams: SearchQuery = {
                sortby,
                options: {
                    page,
                    limit,
                }
            }

            const data: any = await this.PeopleService.getArchiveUserService(searchParams);
            
            res.status(200).json({ data });
         
        } catch (error) {
            console.log(error)
            next(new HttpException(400, 'Cannot make a search something went wrong.'));            
        }
    }


    /** Insert Imported JSON from excel/csv */
    private insertExcelData = async (req: Request, res: Response, next: NextFunction): Promise<Response | void> => {
        
        try {

            const { excel_data, columns_to_fields } = req.body;

            const insertParams: SearchQuery = {
                excel_data,
                columns_to_fields
            }

            // console.log(insertParams)

            const data = await this.PeopleService.insertExcelDataService(insertParams);

            res.status(201).json(data);

        } catch (error) {
            console.log(error)
            next(new HttpException(400, 'Cannot save excel data.'));
        }
    }


    /** Summary */
    private getSummary = async(req: Request, res: Response, next: NextFunction): Promise<Response | void> => {
        try {
            
            const data = await this.PeopleService.getSummary();
            
            res.status(200).json(data);

        } catch (error) {
            console.log(error)
            next(new HttpException(400, 'Cannot get the summary.'));
            
        }
    }

}



export default PeopleController;