import 'dotenv/config';
import 'module-alias/register';
import validateEnv from '@/utils/validateEnv';
import App from './app';
import PeopleController from './resources/people/people.controller';
import CollectionSettingController from './resources/collection_setting/collectionSetting.controller';
// import PostController from '@/resources/post/post.controller';
import UserController from '@/resources/user/user.controller';
import * as Sentry from '@sentry/node';
import * as Tracing from '@sentry/tracing';

validateEnv();

const PEOPLE_CONTROLLER = new PeopleController();
const COLLECTION_SETTING_CONTROLLER = new CollectionSettingController();
const USER_CONTROLLER = new UserController();

const app = new App(
    [
        PEOPLE_CONTROLLER,
        COLLECTION_SETTING_CONTROLLER,
        USER_CONTROLLER
    ],
    Number(process.env.PORT)
);

// app.listen();

Sentry.init({
  dsn: "https://7561707ae0294b29b241f204449cabe0@o1140968.ingest.sentry.io/6205275",
  tracesSampleRate: 1.0,
});

const transaction = Sentry.startTransaction({
  op: "test",
  name: "My First Test Transaction",
});

setTimeout(() => {
  try {
    app.listen();
  } catch (e) {
    Sentry.captureException(e);
  } finally {
    transaction.finish();
  }
}, 99);
